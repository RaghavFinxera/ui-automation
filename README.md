### Finxera - QA InfoTech UI Automation
---
#### System Requirement:

* JDK 1.7 or above

* Maven 3.1 or above

* Eclipse or IDE of choice in case there is need to update the script. (optional)

* For execution of scripts on Chrome or Internet explorer(According to system requirment i.e for 32 bit system use 32 bit driver ) you need to have executable files for both drivers respectively and paste them at location 
\"\src\test\resources\drivers" in project folder.

* You can download these executable files from below links
 
  * Chrome: https://drive.google.com/file/d/0B4FqnK04LJRnNWZFOEE3Wjd4aFk/view
  * Interner Explorer: https://drive.google.com/file/d/0B4FqnK04LJRnbi1nUkc0YzlYUkU/view

Execution Steps :-

Please follow the instructions to execute the tests on local:

1 -According to the Test Scope use following commands in cmd or terminal
  - To Execute the complete test suite

	mvn clean compile -Dparameter=value


##### Browser settings

For IE : 

	* On IE 7 or higher on Windows Vista or Windows 7, you must set the Protected Mode settings for each zone to be the same value. The value can be on or off,
	  as long as it is the same for every zone. To set the Protected Mode settings, choose "Internet Options..." from the Tools menu, and click on the Security 
	  tab. For each zone, there will be a check box at the bottom of the tab labeled "Enable Protected Mode".
	* Additionally, "Enhanced Protected Mode" must be disabled for IE 10 and higher. This option is found in the Advanced tab of the Internet Options dialog.
	  The browser zoom level must be set to 100% so that the native mouse events can be set to the correct coordinates. 
	  for more details follow this link : https://code.google.com/p/selenium/wiki/InternetExplorerDriver

For All : Make sure pop ups are not blocked and make sure browser version is comaptible with selenium version.

## For Email Validation Test case

Make Sure your system and mail server time should be same.