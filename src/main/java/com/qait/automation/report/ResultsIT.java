package com.qait.automation.report;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import static com.qait.automation.utils.DataReadWrite.getProperty;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;


public class ResultsIT {

	String today = new Date().toString();
	String resultOfRun = null;
	String host = "smtp.gmail.com";
	String from_email_id = getProperty("./Config.properties", "from_email_id");
	String password = getProperty("./Config.properties", "password");
	String to_email_id = getProperty("./Config.properties", "to_email_id");
	String[] CC_value;
	//int port = 25;
	int port = 587; //Finxera - changed on Nov-3
	int failureResults = 0;
	int skippedResults = 0;
	int passedResult = 0;
	String setUserComment=null;
	boolean sendResults = false;
	final String projectName = "FinXera";
	public static int count = 0;
	String failedTestBugId;
	String screenshotsPath=null;


	public void sendResultsMail(List<String> testCaseId,List <String> attachmentFilePath,String pathToScreenshotsFolder,int fail,int pass,int skipped,String comment,List<String> bugId) {
		try{
			System.out.println("Inside - sendResultsMail");
			failureResults=fail;
			skippedResults=skipped;
			passedResult=pass;
			setUserComment=comment;    	
			failedTestBugId=bugId.toString();
			screenshotsPath = pathToScreenshotsFolder;

			if (true) {
				Message message = new MimeMessage(getSession());
				message.addFrom(new InternetAddress[]{(new InternetAddress(from_email_id))});
				setMailRecipient(message);
				message.setContent(setAttachment(testCaseId,attachmentFilePath));
				message.setSubject(setMailSubject());
				Session session = getSession();
				//Transport transport = session.getTransport("smtps");
				Transport transport = session.getTransport("smtp");//Finxera - added Nov-3
				transport.connect(host, from_email_id, password);
				transport.sendMessage(message, message.getAllRecipients());
				transport.close();
			}
			System.out.println("Reports emailed");
		}catch(Exception e){
			e.printStackTrace();
		}

	}

	private Session getSession() {



		Authenticator authenticator = new Authenticator(from_email_id, password);
		Properties properties = new Properties();
		properties.setProperty("mail.transport.protocol", "smtps");
		properties.put("mail.smtps.auth", "true");
		properties.setProperty("mail.smtp.submitter", authenticator
				.getPasswordAuthentication().getUserName());
		properties.setProperty("mail.smtp.auth", "true");

		properties.setProperty("mail.smtp.ssl.trust", "smtp.gmail.com");//Finxera - added Nov-3

		properties.setProperty("mail.smtp.starttls.enable", "true");//Finxera - added on Nov-3
		properties.setProperty("mail.smtp.host", host);
		properties.setProperty("mail.smtp.port", String.valueOf(port));
		return Session.getInstance(properties, authenticator);
	}

	private String setBodyText(String testId) throws IOException {
		String mailtext = "";
		if(setUserComment==null){

			mailtext = "<b>Hi All,</b><br>";
			mailtext = mailtext
					+ "</br><b><font color = Black>Please find below the Finxera test automation specifictions & test execution results : </font></b></br>";
			mailtext = mailtext
					+ "<br><b><font style = Courier, color = Black>Test Case/Suite Id: <b><font style = Courier, color = Blue>"
					+ testId+"</font></b>";

			mailtext = mailtext
					+ "<br><b><font color = Black>Browser: </font></b><font color = Blue>"
					+ getProperty("./Config.properties", "browser")+"</font>";
			mailtext = mailtext
					+ "<br><b><font color = Black>Test Case Executed By: </font></b><font color = Blue>"
					+ projectName + " Automation Team</font>";
			mailtext = mailtext
					+ "<br><b><font color = Black>Test Date: </font></b><font color = Blue>" + today+"</font>";

			mailtext = mailtext + "<br>";
			mailtext = mailtext + "<br><font color = Black>=============================================================</font><br>";
			mailtext = mailtext + "<br>";
			mailtext = mailtext + "<b><u><font color = Black>Test Execution Results</u> : </b></font><br>";        

			if(failedTestBugId=="[]"){
				mailtext = mailtext + "<br><table border=\"1\"><tr><td align=\"center\"><b><font color = Black>Test status</font></b></td><td align=\"center\"><b><font color = Black>TestSteps Count</font></b></td><td align=\"center\"><b><font color = Black>JIRA Id</font></b></td></tr>" +
						"<tr><td><b><font color = Blue>TOTAL</font></b></td><td align=\"center\"><b><font color = Blue>"+(failureResults+passedResult+skippedResults)+"</font></b></td><td align=\"center\"><b><font color = Black></font></b></td></tr>" +
						"<tr><td><b><font color = green>PASSED</font></b></td><td align=\"center\"><b><font color = Green>" + passedResult+"</font></b></td><b><font color = Green></font></b><td align=\"center\"></td></tr>" +
						"<tr><td><b><font color = Red>FAILED</font></b></td><td align=\"center\"><b><font color = Red>" + failureResults+"</font></b></td><td align=\"center\"><b><font color = Red></font></b></td></tr>" +
						"<tr><td><b><font color = Orange>SKIPPED</font></b></td><td align=\"center\"><b><font color = Orange>" + skippedResults+"</font><b></td><td align=\"center\"><b><font color = Orange></font></b></td></tr></table><br>";	
			}else{
				if(skippedResults==0){
					mailtext = mailtext + "<br><table border=\"1\"><tr><td align=\"center\"><b><font color = Black>Test status</font></b></td><td align=\"center\"><b><font color = Black>TestSteps Count</font></b></td><td align=\"center\"><b><font color = Black>Jira Id</font></b></td></tr>" +
							"<tr><td><b><font color = Blue>Total Test</font></b></td><td align=\"center\"><b><font color = Blue>"+(failureResults+passedResult+skippedResults)+"</font></b></td><td align=\"center\"><b><font color = Black></font></b></td></tr>" +
							"<tr><td><b><font color = green>PASSED</font></b></td><td align=\"center\"><b><font color = Green>" + passedResult+"</font></b></td><b><font color = Green></font></b><td align=\"center\"></td></tr>" +
							"<tr><td><b><font color = Red>FAILED</font></b></td><td align=\"center\"><b><font color = Red>" + failureResults+"</font></b></td><td align=\"center\"><b><font color = Red>"+failedTestBugId+"</font></b></td></tr>" +
							"<tr><td><b><font color = Orange>SKIPPED</font></b></td><td align=\"center\"><b><font color = Orange>" + skippedResults+"</font><b></td><td align=\"center\"><b><font color = Orange></font></b></td></tr></table><br>";
				}else{
					mailtext = mailtext + "<br><table border=\"1\"><tr><td align=\"center\"><b><font color = Black>Test status</font></b></td><td align=\"center\"><b><font color = Black>TestSteps Count</font></b></td><td align=\"center\"><b><font color = Black>Jira Id</font></b></td></tr>" +
							"<tr><td><b><font color = Blue>Total Test</font></b></td><td align=\"center\"><b><font color = Blue>"+(failureResults+passedResult+skippedResults)+"</font></b></td><td align=\"center\"><b><font color = Black></font></b></td></tr>" +
							"<tr><td><b><font color = green>PASSED</font></b></td><td align=\"center\"><b><font color = Green>" + passedResult+"</font></b></td><b><font color = Green></font></b><td align=\"center\"></td></tr>" +
							"<tr><td><b><font color = Red>FAILED</font></b></td><td align=\"center\"><b><font color = Red>" + failureResults+"</font></b></td><td align=\"center\"><b><font color = Red>"+failedTestBugId+"</font></b></td></tr>" +
							"<tr><td><b><font color = Orange>SKIPPED</font></b></td><td align=\"center\"><b><font color = Orange>" + skippedResults+"</font><b></td><td align=\"center\"><b><font color = Orange>Initial Pre-requisite failed</font></b></td></tr></table><br>";
				}
			}

			mailtext = mailtext + "<font color = Black>=============================================================</font>";

			String pathToScreenshots;
			if(screenshotsPath!=null){
				pathToScreenshots = screenshotsPath.replace(System.getProperty("user.dir"), ".");
				mailtext = mailtext + "<br><b><i>NOTE :</i></b>Captured screenshots are saved in Project WorkSpace within location : <font color = Blue>"+pathToScreenshots.substring(0,pathToScreenshots.lastIndexOf("\\"))+"\\</font><br>";
			}       

			mailtext = mailtext
					+ "<br><font color = Black>Please find detailed test result files in the attachment </font></br>";
			mailtext = mailtext
					+ "<br><font color = Black>Note: This is a system generated mail. Please do not reply.</font>"
					+ "</br>";
			mailtext = mailtext + "<br><br><font color = Black>Thanks & Regards" + "</font></br></br>";
			mailtext = mailtext + "<br>"; 
		}else{
			mailtext = "<b>Hi All,</b><br>";
			mailtext = mailtext
					+ "</br><b><font color = Black>Please find below the Finxera test automation specifictions & test execution results : </font></b></br>";
			mailtext = mailtext
					+ "<br><b><font style = Courier, color = Black>Test Case/Suite Id: <b><font style = Courier, color = Blue>"
					+ testId+"</font></b>";

			mailtext = mailtext
					+ "<br><b><font color = Black>Browser: </font></b><font color = Blue>"
					+ getProperty("./Config.properties", "browser")+"</font>";
			mailtext = mailtext
					+ "<br><b><font color = Black>Test Case Executed By: </font></b><font color = Blue>"
					+ projectName + " Automation Team</font>";
			mailtext = mailtext
					+ "<br><b><font color = Black>Test Date: </font></b><font color = Blue>" + today+"</font>";

			mailtext = mailtext
					+ "<br><br><br><b><font color = Red>"+setUserComment+"</font></b>";
			mailtext = mailtext + "<br><br><font color = Black>Thanks & Regards" + "</font></br></br>";
			mailtext = mailtext + "<br>";
		}

		return mailtext;
	}

	private String setMailSubject() {
		return (projectName + " Automated Test Results: " + today);
	}

	private void setMailRecipient(Message message) throws AddressException, MessagingException, IOException {     
		message.addRecipient(Message.RecipientType.TO, new InternetAddress(to_email_id));
		CC_value = getProperty("./Config.properties", "cc_email_id").split(",");
		for(int i=0;i<CC_value.length;i++)
		{
			message.addRecipient(Message.RecipientType.CC, new InternetAddress(CC_value[i]));
		}
	}

	private Multipart setAttachment(List<String> testId,List<String> attachmentFilePath) throws MessagingException, IOException {
		// Create the message part
		MimeBodyPart messageBodyPart = new MimeBodyPart();

		// Fill the message
		messageBodyPart.setContent(setBodyText(testId.toString()), "text/html");

		MimeMultipart multipart = new MimeMultipart();
		multipart.addBodyPart(messageBodyPart);

		// Part two is attachment
		messageBodyPart = new MimeBodyPart();
		if(setUserComment==null){
			for (int i = 0; i < attachmentFilePath.size(); i++) {
				addAttachment(multipart, messageBodyPart,attachmentFilePath.get(i));	
			}}else{
				System.out.println("No attachment");
			}

		return multipart;
	}

	private static void addAttachment(Multipart multipart,
			MimeBodyPart messageBodyPart, String filename)
					throws MessagingException {
		messageBodyPart = new MimeBodyPart();
		File f = new File(filename);
		DataSource source = new FileDataSource(f);
		messageBodyPart.setDataHandler(new DataHandler(source));
		messageBodyPart.setFileName(f.getName());
		multipart.addBodyPart(messageBodyPart);
	}
}