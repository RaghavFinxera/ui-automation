/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qait.automation;

import static com.qait.automation.utils.DataReadWrite.getProperty;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Reporter;

import com.qait.automation.utils.CustomFunctions;
import com.qait.finxera.keywords.ExecutionKeyWords;

/**
 * @author QAIT
 *
 */
public final class TestSessionInitiator {

    protected WebDriver driver;
    String browser;
    private WebDriverFactory wdfactory;
    String seleniumserver;
    String seleniumserverhost;
    String appbaseurl;
    String applicationpath;
    String chromedriverpath;
    String testurl;
    String datafileloc = "";
    static int timeout;
    Map<String, Object> chromeOptions = null;
    DesiredCapabilities capabilities;
    public ExecutionKeyWords execution_keywords;
    public CustomFunctions custom_functions;

    private void _initPage() {
    	custom_functions = new CustomFunctions(driver);
        execution_keywords = new ExecutionKeyWords(driver);
    }

    public TestSessionInitiator() {
    	testurl = _getSessionConfig().get("testurl");
    	testurl = System.getProperty("testurl",testurl);
        wdfactory = new WebDriverFactory();
        _configureBrowser();
        _initPage();
        launchApplication(testurl);
    }

    private void _configureBrowser() {
        driver = wdfactory.getDriver(_getSessionConfig());
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        driver.manage()
                .timeouts()
                .implicitlyWait(
                        Integer.parseInt(_getSessionConfig().get("timeout")),
                        TimeUnit.SECONDS);
    }

    public Map<String, String> _getSessionConfig() {
    	seleniumserver = System.getProperty("seleniumserver",seleniumserver);
    	seleniumserverhost = System.getProperty("seleniumserverhost",seleniumserverhost);    
    	
        String[] configKeys = {"tier", "browser", "testurl", "seleniumserver",
            "seleniumserverhost", "timeout", "driverpath"};
        Map<String, String> config = new HashMap<>();
        for (String configKey : configKeys) {
            config.put(configKey, getProperty("./Config.properties", configKey));
        }
        return config;
    }

    public String getEnv() {
        String tier = System.getProperty("env");
        if (tier == null) {
            tier = _getSessionConfig().get("tier");
        }
        return tier;
    }

    public String getBrowser() {
        String o_browser = System.getProperty("browser");
        if (o_browser == null) {
            o_browser = _getSessionConfig().get("browser");
        }
        return o_browser;
    }

    public void launchApplication(String applicationpath) {
        Reporter.log("The application url is :- " + applicationpath, true);
        Reporter.log(
                "The test browser is :- " + _getSessionConfig().get("browser"),
                true);
        try {
            driver.get(applicationpath);
            driver.navigate().refresh();	          
        } catch (WebDriverException e) {
        	driver.navigate().refresh();
            driver.get(applicationpath);
            driver.navigate().refresh();
        }
    }

    public void getURL(String url) {
        driver.manage().deleteAllCookies();
        driver.get(url);
    }

    public String getCurrentURL() {
        return driver.getCurrentUrl();
    }

    public void closeBrowserSession() {
        driver.quit();
        Reporter.log("User quits browser session");
    }

    public void closeBrowserWindow() {
        driver.close();
        Reporter.log("User Close acive browser window");
    }

}
