package com.qait.automation.getpageobjects;

import static com.qait.automation.getpageobjects.ObjectFileReader.getELementFromFile;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;

/**
 * @author QAIT
 * 
 */
public class GetPage extends BaseUi {

	protected WebDriver driver;
	String pageName;
	String version;

	public GetPage(WebDriver driver, String pageName) {
		super(driver, pageName);
		this.driver = driver;
		this.pageName = pageName;
	}

	protected void waitForElementToBeVisible(WebElement elementToken) {
		try {
			wait.waitForElementToBeVisible(elementToken);
		} catch (Exception e) {
			logMessage(elementToken + " is not visible with in specific time");
		}
	}

	public WebElement createElementWithParameters(By locator, String ObjectName,String type,String dataPassed) {
		  try {
		   WebElement element = driver.findElement(locator);
		   if(type=="input"||type=="select"){
		   beanForInfo.setComment(ObjectName + " is displayed and data input is :: "+dataPassed);
		   }else{
		    beanForInfo.setComment(ObjectName + " is displayed");
		   }
		   return element;
		  } catch (Exception e) {
		   Reporter.log("Unable to locate element : " + ObjectName, true);
		   beanForInfo.setComment("Unable to locate element : " + ObjectName);
		   return null;
		  }
		 }
	
	public WebElement CreateWebelement(By locator){
		return driver.findElement(locator);
	}
	
	protected WebElement waitForElementToBeClickable(WebElement element) {
		return wait.waitForElementToBeClickable(element);
	}

	protected boolean isElementDisplayed(WebElement elementName, String objectName) {
		try {
			boolean result = elementName.isDisplayed();
			Reporter.log(objectName + " is displayed", true);
			return result;
		} catch (Exception e) {
			Reporter.log(objectName + " is not displayed", true);
			return false;
		}
	}

	protected boolean isElementNotDisplayed(WebElement elementName, String objectName) {
		  try {
		   if(elementName.isDisplayed())
		   Reporter.log(objectName + " is not displayed", true);
		   return false;
		  } catch (NoSuchElementException e) {
			  Reporter.log(objectName + " is displayed", true);
		   return true;
		  } 
		 }
	
	protected boolean verifyElementText(WebElement element, String expectedValue) {
		String actualValue = element.getText().trim();
		boolean flag = false;
		try {
			if(actualValue.contains(expectedValue)){
				flag = true;
				Reporter.log("Actual Value :"+actualValue+" is not equal to expected Value :"+expectedValue);
			}			
		} catch (Exception e) {
		}
		return flag;
	}

	protected By getLocator(String elementToken) {
		String[] locator = getELementFromFile(this.pageName, elementToken);
		return getLocators(locator[1].trim(), locator[2].trim());
	}
	
	public By getLocators(String locatorType, String locatorValue) {
		switch (locatorType) {
		case "id":
			return By.id(locatorValue);
		case "xpath":
			return By.xpath(locatorValue);
		case "name":
			return By.name(locatorValue);
		case "classname":
			return By.className(locatorValue);
		case "css":
			return By.cssSelector(locatorValue);
		case "linktext":
			return By.linkText(locatorValue);
		default:
			return By.xpath(locatorValue);
		}
	}

}