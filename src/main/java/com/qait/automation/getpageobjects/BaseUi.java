/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qait.automation.getpageobjects;

import static com.qait.automation.getpageobjects.ObjectFileReader.getPageTitleFromFile;
import static com.qait.automation.utils.DataReadWrite.getProperty;
import java.util.List;
import java.util.Set;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import com.qait.automation.utils.BeanForinfo;
import com.qait.automation.utils.SeleniumWait;

/**
 * @author QAIT
 * 
 */
public class BaseUi {

	WebDriver driver;
	protected SeleniumWait wait;
	private String pageName;
	public BeanForinfo beanForInfo;
	String window;

	protected BaseUi(WebDriver driver, String pageName) {
		PageFactory.initElements(driver, this);
		this.driver = driver;
		this.pageName = pageName;
		beanForInfo = new BeanForinfo();
		this.wait = new SeleniumWait(driver, Integer.parseInt(getProperty(
				"Config.properties", "timeout")));
	}

	public String getPageTitle() {
		return driver.getTitle();
	}

	public String getElementText(WebElement element) {
		return element.getText();

	}

	public String getCurrentURL() {
		driver.getTitle();
		return driver.getCurrentUrl();
	}

	public boolean verifyPageTitleExact() {
		String pagetitle = getPageTitleFromFile(pageName);
		if (getPageTitle().contains(pagetitle)) {
			Reporter.log("PageTitle for " + pageName + " is exactly: '"
					+ pagetitle + "'.", true);
			return true;
		} else {
			return false;
		}
	}

	public boolean verifyExpectedURL(String expectedURL) {
		if (getCurrentURL().contains(expectedURL)) {
			Reporter.log("Assertion Passed: Current URL :- " + getCurrentURL()
					+ " contains expected URL :- " + expectedURL, true);
			return true;
		} else {
			Reporter.log("Assertion Failed: Current URL :- " + getCurrentURL()
					+ " doesn't contains expected URL :- " + expectedURL, true);
			return false;
		}
	}

	public boolean verifyPageTitle(String pagetitle) {
		wait.waitForPageTitleToContain(pagetitle);
		if (getPageTitle().contains(pagetitle)) {
			Reporter.log("Assertion Passed: PageTitle for " + pagetitle
					+ " is : '" + pagetitle + "'.",true);
			Reporter.log("User is on " + pagetitle + "Page",true);
			return true;
		} else {
			return false;
		}

	}

	public void clickByJavaScriptUsingXpath(WebElement element) {
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",
				element);
	}

	protected WebElement getElementByIndex(List<WebElement> elementlist,
			int index) {
		return elementlist.get(index);
	}

	public void switchToFrame(WebElement element) {
		try {
			hardWait(1);
			driver.switchTo().frame(element);
		} catch (StaleElementReferenceException ex1) {
			hardWait(1);
			driver.switchTo().frame(element);
		}
	}

	public void switchToDefaultContent() {
		hardWait(1);
		driver.switchTo().defaultContent();
	}

	protected void executeJavascript(String script) {
		hardWait(1);
		((JavascriptExecutor) driver).executeScript(script);
		hardWait(1);
	}

	protected void executeJavascript(String script, WebElement element) {
		hardWait(1);
		((JavascriptExecutor) driver).executeScript(script, element);
		hardWait(1);
	}

	protected Actions action() {
		Actions hoverOver = new Actions(driver);
		return hoverOver;
	}

	protected void handleAlert() {
		try {
			switchToAlert().accept();
			Reporter.log("Alert is Handled",true);
			driver.switchTo().defaultContent();
		} catch (Exception e) {
			Reporter.log("Alert did not come",true);
		}
	}

	public Alert switchToAlert() {
		WebDriverWait wait = new WebDriverWait(driver, 5);
		return wait.until(ExpectedConditions.alertIsPresent());
	}

	public String getAlertText() {
		Alert alert = switchToAlert();
		return alert.getText();
	}

	public String getBrowser() {
		String browser;
		browser = getProperty("./Config.properties", "browser");
		return browser;
	}

	public String getTier() {
		String tier;
		tier = getProperty("./Config.properties", "tier");
		return tier;
	}

	public boolean backToMainWindow() {
		hardWait(2);
		try {
			Set<String> handles = driver.getWindowHandles();
			for (String handle : handles) {
				driver.switchTo().window(handle);
			}
			System.out.println(driver.getTitle());
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean changeWindowHandle() {
		try {
			window = driver.getWindowHandle();
			while (driver.getWindowHandles().size() == 1) {
				hardWait(1);
			}
			System.out.println(driver.getWindowHandles().size());
			hardWait(2);
			Set<String> handles = driver.getWindowHandles();
			for (String handle : handles) {
				if (!handle.equals(window)) {
					driver.switchTo().window(handle);
					driver.switchTo().window(window);
					driver.switchTo().window(handle);
				}
			}
			driver.manage().window().maximize();
			System.out.println(driver.getTitle());
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public void hardWait(int seconds) {
		try {
			Thread.sleep(seconds * 1000);
		} catch (InterruptedException ex) {
			ex.printStackTrace();
		}
	}

	public void clearCookies() {
		driver.manage().deleteAllCookies();
	}

	public void selectProvidedTextFromDropDownByVisibleText(WebElement el, String text) {
		Select sel = new Select(el);
		sel.selectByVisibleText(text);
	}
	
	public void selectProvidedTextFromDropDownByValue(WebElement el, String text) {
		Select sel = new Select(el);
		sel.selectByValue(text);
	}

	public void hoverOverElement(WebElement ele) {
		Actions hoverOver = new Actions(driver);
		hoverOver.moveToElement(ele).build().perform();
	}

	protected void clickUsingActionScript(WebElement e) {
		Actions element = new Actions(driver);
		element.click().perform();
	}

	protected void logMessage(String message) {
		Reporter.log(message, true);
	}

	public void ClickOnElement(WebElement element) {
		element.click();
	}

	public void EnterData(WebElement element, String text) {
		element.clear();
		element.sendKeys(text);
	}

	public String GetText(WebElement element) {
		return element.getText();
	}
	
	public void replaceClick(String identifier, String value) {
		identifier = identifier.replaceAll("#", value);
		driver.findElement(By.xpath(identifier)).click();
	}

	public void refreshPage() {
		driver.navigate().refresh();
	}
	
	public void waitForElementToBeEnabled(WebElement element) {
		while (!element.isEnabled()) {
			hardWait(2);
		}
	}

	public void verifyText(WebElement element, String text) {
		String expectedText = element.getText();
		Assert.assertEquals(expectedText, text,
				"Assertion Failed : Text does't Match");
		logMessage("Assertion Passed : " + text + " is correct");
	}
}