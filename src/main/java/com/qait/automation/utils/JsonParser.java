package com.qait.automation.utils;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 * 
 * @author prashantshukla
 */
public class JsonParser {

	public JSONArray getJsonValue(String jsonString, String jsonKey)
			throws JSONException {
		JSONObject obj = new JSONObject(jsonString);
		JSONArray issuesArray = obj.getJSONArray(jsonKey.split(":")[0]);
		return issuesArray;
	}
}
