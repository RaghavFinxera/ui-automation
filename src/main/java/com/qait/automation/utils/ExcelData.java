package com.qait.automation.utils;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.util.CellRangeAddress;

/**
 * @author QAIT
 * 
 */
public class ExcelData {

	public BeanForinfo beanInfo = new BeanForinfo();
	private HSSFSheet ExcelWSheet;
	private HSSFWorkbook ExcelWBook;
	private HSSFCell Cell;
	ArrayList<String> cellValue;
	private static HSSFSheet sh;
	private static HSSFWorkbook wb;
	HSSFCellStyle style;
	HSSFFont font;
	Row row;

	// Read excel
	public void setExcelFile(String path) {

		try {
			FileInputStream ExcelFile = new FileInputStream(path);
			ExcelWBook = new HSSFWorkbook(ExcelFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public int getNumMergedRegions() {
		return ExcelWSheet.getNumMergedRegions();
	}

	public int getMergedRegionColumn(int index) {

		return ExcelWSheet.getMergedRegion(index).getFirstColumn();
	}

	public int getMergedRegionRow(int index) {

		return ExcelWSheet.getMergedRegion(index).getFirstRow();
	}

	public void setSheetToIndex(int index) {
		ExcelWSheet = ExcelWBook.getSheetAt(index);
	}

	public String getSheetName(int i) {
		return ExcelWBook.getSheetName(i);
	}

	public int getSheetNameIndex(String sheetName) {
		return ExcelWBook.getNameIndex(sheetName);

	}

	public int getTotalNumberOfSheets() {
		return ExcelWBook.getNumberOfSheets();
	}

	// Get value from Excel Sheet
	public String getCellData(int ColNum, int RowNum) throws Exception {
		try {
			Cell = ExcelWSheet.getRow(RowNum).getCell(ColNum);
			String CellData = Cell.getStringCellValue();
			return CellData;
		} catch (Exception e) {
			return "";
		}
	}

	// Number of Rows
	public int rows() {
		return ExcelWSheet.getLastRowNum();
	}

	public int getColumnCount() {
		return ExcelWSheet.getRow(0).getLastCellNum();
	}

	// New sheet generation
	public void createSheet() {
		try {
			wb = new HSSFWorkbook();
			sh = wb.createSheet("Result Sheet");
			setlabels();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Set value in new excel sheet
	public void setValues(int Row, String status, String jiraBugId,
			String comment, String testcaseDescription) {
		try {
			style = wb.createCellStyle();
			font = wb.createFont();
			style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
			style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			style.setBorderTop(HSSFCellStyle.BORDER_THIN);
			style.setBorderRight(HSSFCellStyle.BORDER_THIN);
			style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			
			row = sh.createRow(Row);

			for (int i = 0; i < cellValue.size(); i++) {				
				
				Cell cell = row.createCell(i);
				cell.setCellType(org.apache.poi.ss.usermodel.Cell.CELL_TYPE_STRING);

				if (cellValue.get(i).contains("Test Case Description")) {
					cell.setCellValue(testcaseDescription);
					sh.autoSizeColumn(i);
					if (status.isEmpty()) {
						if (testcaseDescription.isEmpty() && comment.isEmpty()) {
							style.setFillForegroundColor(HSSFColor.DARK_YELLOW.index);
							cell.setCellStyle(style);
						} else {
							style.setFillForegroundColor(HSSFColor.YELLOW.index);
							font.setFontHeightInPoints((short) 10);
							font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
							font.setFontName(HSSFFont.FONT_ARIAL);							
							sh.addMergedRegion(new CellRangeAddress(Row, Row,
									0, cellValue.size() - 1));
							style.setFont(font);
							cell.setCellStyle(style);							
						}
					}
				} else if (cellValue.get(i).contains("Result Status")) {
					cell.setCellValue(status);
					sh.autoSizeColumn(i);
					if (status.contains("PASS")) {
						style.setFillForegroundColor(HSSFColor.GREEN.index);
						cell.setCellStyle(style);
					} else if (status.contains("FAIL")) {
						style.setFillForegroundColor(HSSFColor.RED.index);
						cell.setCellStyle(style);
					} else if (status.contains("SKIPPED")) {
						style.setFillForegroundColor(HSSFColor.YELLOW.index);
						cell.setCellStyle(style);
					} else {
						style.setFillForegroundColor(HSSFColor.BLUE_GREY.index);
						cell.setCellStyle(style);
					}
				} else if (cellValue.get(i).contains("JIRA Id")) {
					if (jiraBugId != "[]") {
						cell.setCellValue(jiraBugId);
					}					
					sh.autoSizeColumn(i);
				} else if (cellValue.get(i).contains("Execution Comment")) {
					cell.setCellValue(comment);
					sh.autoSizeColumn(i);
					if (comment.isEmpty()) {
						style.setFillForegroundColor(HSSFColor.YELLOW.index);
						cell.setCellStyle(style);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setlabels() {
		try {

			style = wb.createCellStyle();
			style.setFillForegroundColor(HSSFColor.DARK_BLUE.index);
			style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
			style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			style.setBorderTop(HSSFCellStyle.BORDER_THIN);
			style.setBorderRight(HSSFCellStyle.BORDER_THIN);
			style.setBorderLeft(HSSFCellStyle.BORDER_THIN);		
			
			font = wb.createFont();
			font.setColor(HSSFColor.WHITE.index);
			font.setFontHeightInPoints((short) 11);
			font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			font.setFontName(HSSFFont.FONT_ARIAL);
			style.setFont(font);

			row = sh.createRow(0);

			cellValue = new ArrayList<String>();
			cellValue.add("Test Case Description");
			cellValue.add("Result Status");
			cellValue.add("JIRA Id");
			cellValue.add("Execution Comment");

			for (int i = 0; i < cellValue.size(); i++) {
				Cell cell = row.createCell(i);
				cell.setCellType(org.apache.poi.ss.usermodel.Cell.CELL_TYPE_STRING);
				cell.setCellValue(cellValue.get(i));
				cell.setCellStyle(style);
				sh.autoSizeColumn(i);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Close final result sheet
	public void final_write_results(String path) {
		FileOutputStream fos;
		try {
			fos = new FileOutputStream(path);
			wb.write(fos);
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
