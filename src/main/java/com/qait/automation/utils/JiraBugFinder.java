package com.qait.automation.utils;

import java.io.IOException;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.testng.Reporter;
import com.qait.automation.utils.HttpClient;
import com.qait.automation.utils.JsonParser;
import com.sun.jersey.api.client.ClientResponse;

/**
 *
 * @author QAIT
 */
public class JiraBugFinder {

	private final String jiraStoryResource;
	String entity;
	String jiraStoryId = "FIN";
	
	public JiraBugFinder() {
		this.jiraStoryResource = Constants.JIRA_URL + Constants.JIRA_SEARCH_ISSUE_JQL + jiraStoryId
				+ "%20AND%20issuetype%20=%20bug&os_username=" + Constants.JIRA_USERNAME + "&os_password="
				+ Constants.JIRA_PASSWORD;
	}

	public String getStoryUrl() {
		return this.jiraStoryResource;
	}

	public JSONArray getBugs() throws IOException, JSONException {
		HttpClient httpclient = new HttpClient();
		ClientResponse response = httpclient.getHttpResponse(this.jiraStoryResource);
		entity = response.getEntity(String.class);
		return new JsonParser().getJsonValue(entity, "issues");
	}

	
	
	public String createABug(String bug_Summary) throws JSONException {
		String resourceURL = Constants.JIRA_URL + Constants.JIRA_ISSUE;

		Object postBody = ("{\"fields\": {\"project\": {\"id\": \"10000\"}, \"summary\": \"" + bug_Summary
				+ "\",\"issuetype\": {\"id\": \"1\"}, \"assignee\": { \"name\": \"test-automation\" },	\"priority\": {\"id\": \"3\"},\"labels\": [\"bug\"]}}");

		HttpClient httpclient = new HttpClient();
		ClientResponse response = httpclient.postHttpResponse(resourceURL, postBody);
		String entity1 = response.getEntity(String.class);
		StringBuilder string1 = new StringBuilder(entity1);
		string1 = string1.append("]");
		string1 = string1.insert(0, "[");
		entity1 = string1.toString();
		JSONArray array1 = new JSONArray(entity1);
		return (String) array1.getJSONObject(0).get("key");
	}

	public String check_create_Bug(String bug_Summary_Line) throws IOException, JSONException {		
		JiraBugFinder jirajBehave = new JiraBugFinder();
		int bugCountNumber = 0;
		String existing_bugId = null;
		String bug_key = null;
		JSONArray entityValue = jirajBehave.getBugs();

		for (int i = 0; i < entityValue.length(); i++) {
			if (entityValue.getJSONObject(i).getJSONObject("fields").getString("summary").contains(bug_Summary_Line)) {
				existing_bugId = entityValue.getJSONObject(i).getString("key");
				bugCountNumber++;
			}
		}
		if (bugCountNumber == 1) {
			bug_key=existing_bugId;
			Reporter.log("Bug with summary :::: "+bug_Summary_Line + " ::::: already exist in Jira with bug id :::: " + existing_bugId,true);
		} else if (bugCountNumber == 0) {
			bug_key = jirajBehave.createABug(bug_Summary_Line);
			Reporter.log("Bug created :: \"" + bug_Summary_Line + "\" :::  and Bug id is ::: " + bug_key, true);
		}
		return bug_key;
	}

}