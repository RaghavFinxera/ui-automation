package com.qait.automation.utils;

public final class Constants {

	public static final String JIRA_URL = System.getProperty("jirahost", "http://10.0.20.227:8080");
	 public static final String JIRA_USERNAME = "test-automation";
	 public static final String JIRA_PASSWORD = "Qait@123";
	 public static final String JIRA_PROJECT_ID = "FIN";
	 public static String bug = "Bug";
	 public static final String JIRA_COMMENT = "comment/";
	 public static final String JQL_STORY_SUMMARY = "&fields=summary";
	 public static final String JIRA_SEARCH_ISSUE_JQL = "/rest/api/2/search?jql=project=";
	 public static final String JIRA_ISSUE = "/rest/api/2/issue/";
}
