package com.qait.automation.utils;


/**
 * @author QAIT
 * 
 */
public class BeanForinfo {

	private String status = " ";
	private String comment = " ";
	
	
	public void setStatus(String status){
		this.status = status;
	}
	
	public void setComment(String comment){
		this.comment = comment;
	}
	
	public String getStatus(){	
		return status;
	}
	
	public String getComment(){
		
		return comment;
	}
	
}
