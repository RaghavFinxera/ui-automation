package com.qait.automation.utils;

@SuppressWarnings("serial")
public class WrongInputException extends Exception {

	public WrongInputException(String s) {
		super(s);
	}

	
}