package com.qait.finxera.keywords;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;
import com.qait.automation.TestSessionInitiator;
import com.qait.automation.getpageobjects.GetPage;
import com.qait.automation.report.ResultsIT;
import com.qait.automation.utils.BeanForinfo;
import static com.qait.automation.utils.DataReadWrite.getProperty;
import com.qait.automation.utils.CustomFunctions;
import com.qait.automation.utils.ExcelData;
import com.qait.automation.utils.JiraBugFinder;
import com.qait.automation.utils.StringContainer;
import com.qait.automation.utils.WrongInputException;

import br.eti.kinoshita.testlinkjavaapi.TestLinkAPI;
import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;
import br.eti.kinoshita.testlinkjavaapi.model.Attachment;
import br.eti.kinoshita.testlinkjavaapi.model.TestCase;
import br.eti.kinoshita.testlinkjavaapi.model.TestPlan;
import br.eti.kinoshita.testlinkjavaapi.util.TestLinkAPIException;

/**
 * @author QAIT
 * 
 */

public class ExecutionKeyWords extends GetPage {

	// Variables and class initialization
	WebDriver driver;
	TestSessionInitiator test;
	JiraBugFinder jiraBugFinder;
	CustomFunctions customFunction;
	ResultsIT emailResults;
	BeanForinfo beanInfo;
	boolean flag = false;
	ExcelData excelData, resultExcelData;
	int failCount = 0, passCount = 0, skippedTestcount = 0;
	String pathToExcelFiles = "./src/main/resources/testdata/DataFile/",
			pathtoResultsFile = "./src/main/resources/testdata/ResultFile/";
	String nameOfDownloadedFile, nameOfPreRequisiteInDownloadedFile, testDataSelectionValue, testCaseIdFormSystemProps,
			testlinkIdUnderPrerequisiteSection, testCaseExecutionStatus, testSteps_action,
			testSteps_testDataLogicalName, testData_testDataToBePassed, dynamicValue_clientId,
			attachment_resultsFilePath, suiteExecutionStatus, testCaseDescription, send_email;
	static ArrayList<Integer> locationOfMergedRow;
	ArrayList<String> preRequisiteList, excelsheetNames, testSteps_Object, testSteps_Actions, testSteps_Data,
			validationSet, validationList, statusList, finalcommentList, stepNameList, finalValueForPre_Condition,
			pathOfAllExcelFileDownloaded, listofTestLinkIdsForEmail, listOfBugs, testId, suiteTCId, suiteTCName,
			suiteTCPostId;
	static HashMap<String, String> dataSetList, validationsSet, validatePageObjectMap, ValidationActions,
			validationData, validationNumberIdentifier, storeDynamicData, excelNameTestDataMap,
			validationObjectTestDataMap, suiteExecutionPassFail, suiteExecutioncomment;
	static HashMap<String, By> webElementListForTestSteps_Object, validationsPageObject_IdentifiersLocator;
	By individualElement;
	int rowForExcelSheeetMain = 1, preRequisiteValue, testSuiteId;
	String excelFileName = null;
	String takeScreenshotStatus = getProperty("./Config.properties", "screenshotStatus");
	String testurl = getProperty("./Config.properties", "testurl");
	String testPlanName = getProperty("./Config.properties", "testPlanName");
	String projectName = getProperty("./Config.properties", "projectName");
	String planId = getProperty("./Config.properties", "testPlanId");
	String testcaseId = getProperty("./Config.properties", "testcaseId");
	String testSuiteExecution = getProperty("./Config.properties", "testSuite");
	String excelFileNameFromConfig = getProperty("./Config.properties", "excelFileName");
	String upload_results_to_testlink = getProperty("./Config.properties", "upload_results_to_testlink");
	String testLinkURL = getProperty("./Config.properties", "testLinkURL");
	String testLinkDevKey = getProperty("./Config.properties", "testLinkDevKey");

	// Constructor to the class
	public ExecutionKeyWords(WebDriver driver) {
		super(driver, "ExecutionKeyWords");
		this.driver = driver;
		beanInfo = new BeanForinfo();
		jiraBugFinder = new JiraBugFinder();
		customFunction = new CustomFunctions(driver);
		excelData = new ExcelData();
		emailResults = new ResultsIT();
		resultExcelData = new ExcelData();
		testSteps_Object = new ArrayList<String>();
		testId = new ArrayList<String>();
		validationSet = new ArrayList<String>();
		finalValueForPre_Condition = new ArrayList<String>();
		listOfBugs = new ArrayList<String>();
		suiteTCId = new ArrayList<String>();
		suiteTCName = new ArrayList<String>();
		suiteTCPostId = new ArrayList<String>();
		validationList = new ArrayList<String>();
		statusList = new ArrayList<String>();
		finalcommentList = new ArrayList<String>();
		stepNameList = new ArrayList<String>();
		ValidationActions = new HashMap<String, String>();
		validationObjectTestDataMap = new HashMap<String, String>();
		testSteps_Actions = new ArrayList<String>();
		testSteps_Data = new ArrayList<String>();
		storeDynamicData = new HashMap<String, String>();
		validationsSet = new HashMap<String, String>();
		validationNumberIdentifier = new HashMap<String, String>();
		validationData = new HashMap<String, String>();
		validatePageObjectMap = new HashMap<String, String>();
		excelNameTestDataMap = new HashMap<String, String>();
		suiteExecutionPassFail = new HashMap<String, String>();
		suiteExecutioncomment = new HashMap<String, String>();
		validationsPageObject_IdentifiersLocator = new HashMap<String, By>();
		pathOfAllExcelFileDownloaded = new ArrayList<String>();
		listofTestLinkIdsForEmail = new ArrayList<String>();
	}

	// ////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * The main function from which Test Case execution starts
	 * 
	 * @throws Exception
	 */
	public void run_function(Object testinitiator) throws Exception {
		// Option to get values for Parameters at Run-time
		testCaseIdFormSystemProps = System.getProperty("testcaseId", testcaseId);
		suiteExecutionStatus = System.getProperty("testSuite", testSuiteExecution);
		planId = System.getProperty("planId", planId);
		testurl = System.getProperty("testurl", testurl);
		testPlanName = System.getProperty("testPlanName", testPlanName);
		projectName = System.getProperty("projectName", projectName);
		takeScreenshotStatus = System.getProperty("screenshotStatus", takeScreenshotStatus);
		upload_results_to_testlink = System.getProperty("resultsUploadStatus",
				upload_results_to_testlink);
		send_email = System.getProperty("send_email", getProperty("./Config.properties", "send_email"));
		testLinkURL = System.getProperty("testLinkURL", testLinkURL);
		testLinkDevKey = System.getProperty("devKey", testLinkDevKey);

		// Following code executes if suite execution status is set to 'Y'
		if (suiteExecutionStatus.contains("Y")) {
			int totalTestsCases = getAllListOfTestCaseUnderTestPlan();

			System.out.println(suiteTCId);
			System.out.println(suiteTCName);

			for (int i = 0; i < totalTestsCases; i++) {
				Reporter.log(":::::::::::::::::::::CODE STARTED:::::::::::::::::::::::::::::::", true);
				Reporter.log("::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::", true);
				resultExcelData.createSheet();
				testCaseIdFormSystemProps = suiteTCId.get(i);
				nameOfDownloadedFile = suiteTCName.get(i);
				excelFileName = suiteTCName.get(i);
				this.driver.manage().deleteAllCookies();
				test = (TestSessionInitiator) testinitiator;
				test.launchApplication(testurl);
				this.driver.navigate().refresh();
				singleExcelRun();
				clearAllListsAndMap();
				resultExcelData.final_write_results(pathtoResultsFile);
				// mark email ....
				listofTestLinkIdsForEmail.add(testCaseIdFormSystemProps);
				pathOfAllExcelFileDownloaded.add(pathtoResultsFile);
				pathtoResultsFile = "./src/main/resources/testdata/ResultFile/";
				rowForExcelSheeetMain = 1;
			}
			Reporter.log("Fail Count is :: " + failCount + " and Pass count is :: " + passCount
					+ " Skipped count is ::: " + skippedTestcount, true);
		} else { // Following code executes if suite execution status is set to
					// 'N'
			resultExcelData.createSheet();
			nameOfDownloadedFile = System.getProperty("excelFileName", excelFileNameFromConfig);
			singleExcelRun();
			listofTestLinkIdsForEmail.add(testCaseIdFormSystemProps);
			pathOfAllExcelFileDownloaded.add(pathtoResultsFile);
		}

		// Following code executes if send email status is set to 'Y'
		if (send_email.contains("Y")) {
			emailResults.sendResultsMail(listofTestLinkIdsForEmail, pathOfAllExcelFileDownloaded,
					attachment_resultsFilePath, failCount, passCount, skippedTestcount, null, listOfBugs);
			listOfBugs.clear();
		}
		listofTestLinkIdsForEmail.clear();
		pathOfAllExcelFileDownloaded.clear();
		if (failCount > 0)
			throw new WrongInputException("There was a failure in the build due to failure of test cases");
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/**
	 * Function to execute excel files irrespective of pre-requisites
	 * 
	 * @throws Exception
	 */
	public void singleExcelRun() throws Exception {
		getExcelFilesFromTestLink(testCaseIdFormSystemProps, nameOfDownloadedFile);
		read_Main_File();
		readPrequisitesFromMainFile();
		checkIfExcelFileIsAlreadyDownloaded();
		Reporter.log("Total excel sheet needs to be executed are  ::: " + preRequisiteList.size(), true);
		int i = 0;

		do {
			nameOfPreRequisiteInDownloadedFile = preRequisiteList.get(i);

			testDataSelectionValue = excelNameTestDataMap.get(nameOfPreRequisiteInDownloadedFile);
			Reporter.log("************************************************************************", true);
			Reporter.log("Execution started for :::::: " + nameOfPreRequisiteInDownloadedFile, true);
			Reporter.log("************************************************************************", true);
			resultExcelData.setValues(rowForExcelSheeetMain, "", "", "",
					nameOfPreRequisiteInDownloadedFile.replace(".xls", "").trim());
			rowForExcelSheeetMain++;
			execute_excel_test_cases(nameOfPreRequisiteInDownloadedFile);
			manipulateStatusList();

			if (testCaseExecutionStatus == "PASS") {

				resultExcelData.setValues(rowForExcelSheeetMain, testCaseExecutionStatus, listOfBugs.toString(),
						finalcommentList.toString(),
						nameOfPreRequisiteInDownloadedFile.replace(".xls", "").trim().concat(stepNameList.toString()));
				updateResultToTestLink(testCaseIdFormSystemProps, planId, testCaseExecutionStatus,
						finalcommentList.toString());
			} else {
				finalcommentList.add(attachment_resultsFilePath);

				resultExcelData.setValues(rowForExcelSheeetMain, testCaseExecutionStatus, listOfBugs.toString(),
						finalcommentList.toString(), nameOfPreRequisiteInDownloadedFile.replace(".xls", "//").trim()
								.concat(stepNameList.toString()));
				suiteExecutionPassFail.put(nameOfPreRequisiteInDownloadedFile, testCaseExecutionStatus);
				suiteExecutioncomment.put(nameOfPreRequisiteInDownloadedFile, finalcommentList.toString());
				updateResultToTestLink(testCaseIdFormSystemProps, planId, testCaseExecutionStatus,
						finalcommentList.toString());
				updateAttachementToTestLink(testCaseIdFormSystemProps, attachment_resultsFilePath);
			}
			rowForExcelSheeetMain += 2;
			if (testCaseExecutionStatus == "FAIL") {
				for (int j = i + 1; j < preRequisiteList.size(); j++) {
					String preRequisiteName1 = preRequisiteList.get(j);
					skippedTestcount++;
					suiteExecutionPassFail.put(nameOfPreRequisiteInDownloadedFile, testCaseExecutionStatus);
					suiteExecutioncomment.put(nameOfPreRequisiteInDownloadedFile, "Initial Pre-requisite failed");
					resultExcelData.setValues(rowForExcelSheeetMain, "SKIPPED", "", "Initial Pre-requisite failed",
							preRequisiteName1.replace(".xls", "").trim());
					rowForExcelSheeetMain++;
				}
				break;
			}
			clearAllListsAndMap();
			i++;
		} while (i < preRequisiteList.size());

		if (!suiteExecutionStatus.contains("Y"))
			Reporter.log("Fail Count is :: " + failCount + " and Pass count is :: " + passCount
					+ " Skipped count is ::: " + skippedTestcount, true);
		resultExcelData.final_write_results(createResultsFile(nameOfDownloadedFile.replace(".xls", "").trim()));
	}

	/**
	 * @return
	 * @throws TestLinkAPIException
	 * @throws MalformedURLException
	 */
	@SuppressWarnings("deprecation")
	private int getAllListOfTestCaseUnderTestPlan() throws TestLinkAPIException, MalformedURLException {
		TestLinkAPI api = new TestLinkAPI(new URL(testLinkURL), testLinkDevKey);
		TestPlan testplanId = api.getTestPlanByName(testPlanName, projectName);
		Date date = new Date();
		TestCase[] testcase = api.getTestCasesForTestPlan(testplanId.getId(), null, null, null, null, null, null, null,
				null, null, null);
		planId = testplanId.getId().toString();
		for (int i = 0; i < testcase.length; i++) {
			int TCID = testcase[i].getId();
			suiteTCPostId.add(String.valueOf(TCID));
			Attachment[] attachment = api.getTestCaseAttachments(testcase[i].getId(), 1);
			String attachementName = null;
			for (int j = 0; j < attachment.length; j++) {
				attachementName = attachment[j].getFileName();
				String current_year = String.valueOf(date.getYear());
				if (!(attachementName.contains(current_year)))
					suiteTCName.add(attachementName);
			}
			String TCExternalId = testcase[i].getFullExternalId();
			suiteTCId.add(TCExternalId);
		}
		return suiteTCId.size();
	}

	/**
	 * Function to download excel files from TestLink
	 * 
	 * @param testLinkId
	 * @param excelSheetFileName
	 * @throws TestLinkAPIException
	 * @throws MalformedURLException
	 * @throws WrongInputException
	 */
	private void getExcelFilesFromTestLink(String testLinkId, String excelSheetFileName)
			throws WrongInputException, MalformedURLException {

		TestLinkAPI api = new TestLinkAPI(new URL(testLinkURL), testLinkDevKey);
		String message = null;
		TestCase testcase = null;
		try {
			testcase = api.getTestCaseByExternalId(testLinkId, 1);
			testId.add(testLinkId);

			Attachment[] attachment = api.getTestCaseAttachments(testcase.getId(), 1);
			int countNumber = 0;
			for (int i = 0; i < attachment.length; i++) {
				if (attachment[i].getFileName().contains(excelSheetFileName)) {
					byte dearr[] = Base64.getDecoder().decode(attachment[0].getContent());
					FileOutputStream fos = new FileOutputStream(
							new File(pathToExcelFiles.concat((attachment[i].getFileName()))));
					fos.write(dearr);
					fos.close();
					countNumber++;
				}
			}
			if (!(countNumber > 0)) {
				Reporter.log(
						"Unable to find " + excelSheetFileName + " in " + testLinkId + ". Hence quiting the code!!!",
						true);
				api.setTestCaseExecutionResult(testcase.getId(), null, Integer.parseInt(planId),
						ExecutionStatus.BLOCKED, null, null,
						"Unable to find " + excelSheetFileName + " in " + testLinkId + ". Hence quiting the code!!!",
						true, null, null, null, null, false);
				emailResults.sendResultsMail(testId, null, null, 0, 0, 0,
						"Unable to find " + excelSheetFileName + " in " + testLinkId + ". Hence quiting the code!!!",
						listOfBugs);
				flag = true;
				message = "Unable to find " + excelSheetFileName + " in " + testLinkId + ". Hence quiting the code!!!";
			}
		} catch (Exception e) {
			Reporter.log("Unable to find TestLink Id :: " + testLinkId + ". Hence exiting the code.", true);
			resultExcelData.setValues(1, "FAIL", "",
					"Unable to find TestLink Id :: " + testLinkId + ". Hence exiting the code.", "");
			resultExcelData.final_write_results(createResultsFile(nameOfDownloadedFile.replace(".xls", "").trim()));
			emailResults.sendResultsMail(testId, null, null, 0, 0, 0,
					"Unable to find TestLink Id :: " + testLinkId + ". Hence exiting the code.", listOfBugs);
			flag = true;
			message = "Unable to find TestLink Id :: " + testLinkId + ". Hence exiting the code.";
		}
		if (flag)
			throw new WrongInputException(message);
	}

	/**
	 * Function to read Main Test case file
	 * 
	 * @throws Exception
	 */
	private void read_Main_File() throws Exception {
		String generatePathToMainExcel = pathToExcelFiles.concat(nameOfDownloadedFile);
		Reporter.log(("Path to download excel File :: " + generatePathToMainExcel), true);
		excelData.setExcelFile(generatePathToMainExcel);
		excelData.setSheetToIndex(0);
	}

	/**
	 * Function to read Pre-requisites from main Test case excel file based on
	 * download flag. Pre-requisite excel files will be downloaded only if flag
	 * is set to "Y".
	 * 
	 * @throws Exception
	 */
	private void readPrequisitesFromMainFile() throws Exception {
		preRequisiteList = new ArrayList<String>();
		preRequisiteValue = getRowIndexOfDesiredText("Pre-requisites");
		String allowUserToDowloadFile;
		getIndexOfMergedRow();

		if (preRequisiteValue != 0) {
			for (int i = preRequisiteValue + 1; i < locationOfMergedRow.get(1) - 1; i++) {
				testlinkIdUnderPrerequisiteSection = excelData.getCellData(0, i);
				excelFileName = excelData.getCellData(1, i);
				testDataSelectionValue = excelData.getCellData(3, i); // Get
																		// TestData
				// value for
				// Pre-Requisites
				// from Main
				// file

				if (testlinkIdUnderPrerequisiteSection.isEmpty()) {
					updateResultToTestLink(testCaseIdFormSystemProps, planId, "BLOCKED",
							"TestLink Id under pre-requisuites found NULL..... Hence quiting the code");
					emailResults.sendResultsMail(testId, null, null, 0, 0, 0,
							"TestLink Id under pre-requisuites found NULL..... Hence quiting the code", listOfBugs);
					throw new WrongInputException(
							"TestLink Id under pre-requisuites found NULL..... Hence quiting the code");
				}
				excelNameTestDataMap.put(excelFileName, testDataSelectionValue);
				allowUserToDowloadFile = excelData.getCellData(2, i);

				if (allowUserToDowloadFile.contains("Y") || allowUserToDowloadFile.contains("y")
						|| allowUserToDowloadFile.contains("yes")) {
					Reporter.log(("TestLinkId is :: " + testlinkIdUnderPrerequisiteSection
							+ " and attached excel workbook name is :: " + excelFileName), true);
					getExcelFilesFromTestLink(testlinkIdUnderPrerequisiteSection, excelFileName);
					preRequisiteList.add(excelFileName);
				} else {
					Reporter.log((" Excel workbook :: " + excelFileName
							+ " is not downloaded, since the download option is marked as N"), true);
					preRequisiteList.add(excelFileName);
				}
			}
			preRequisiteList.add(nameOfDownloadedFile);
		} else {
			int row = getRowIndexOfDesiredText("DataColumnName");
			int column = getColumnIndexOfDesiredText("DataColumnName");
			testDataSelectionValue = excelData.getCellData(column, row + 1);
			excelNameTestDataMap.put(nameOfDownloadedFile, testDataSelectionValue);
			// Reporter.log(("No pre-requisite found"), true);
			preRequisiteList.add(nameOfDownloadedFile);
		}
	}

	/**
	 * Checks if the file to be downloaded already exists in the directory. If
	 * flag is set to "N" and file exist in directory then execution will
	 * continue else execution will terminate
	 * 
	 * @throws WrongInputException
	 * 
	 */
	private void checkIfExcelFileIsAlreadyDownloaded() throws WrongInputException {

		File dir = new File(pathToExcelFiles);
		String[] files = dir.list();
		int j = 0;
		for (int i = 0; i < files.length; i++) {
			if ((preRequisiteList.toString().contains(files[i]))) {
				j++;
			} else {
				continue;
			}
		}
		if (!(j == preRequisiteList.size())) {
			resultExcelData.setValues(1, "FAIL", "", "Please download excel file first.....  Code HAULTING...!!!", "");
			resultExcelData.final_write_results(createResultsFile(nameOfDownloadedFile.replace(".xls", "").trim()));
			updateResultToTestLink(testlinkIdUnderPrerequisiteSection, planId, "Blocked",
					"Please download excel file first.....  Code HAULTING...!!!");
			emailResults.sendResultsMail(listofTestLinkIdsForEmail, null, null, 0, 0, 0,
					"Please download excel file first.....  Code HAULTING...!!!", listOfBugs);
			throw new WrongInputException("Please download excel file first.....  Code HAULTING...!!!");
		}
	}

	/**
	 * Function to update Tst Case Results to corresponding Test Link Id
	 * 
	 * @param testcaseId
	 * @param planId
	 * @param status
	 * @param comment
	 */
	private void updateResultToTestLink(String testcaseId, String planId, String status, String comment) {
		if (upload_results_to_testlink.contains("Y")) {
			try {
				TestLinkAPI api = new TestLinkAPI(new URL(testLinkURL), testLinkDevKey);
				TestCase testcase = api.getTestCaseByExternalId(testcaseId, 1);
				if (status.contains("PASS")) {
					api.setTestCaseExecutionResult(testcase.getId(), null, Integer.parseInt(planId),
							ExecutionStatus.PASSED, null, null, comment, true, null, null, null, null, false);
				} else if (status.contains("FAIL")) {
					api.setTestCaseExecutionResult(testcase.getId(), null, Integer.parseInt(planId),
							ExecutionStatus.FAILED, null, null, comment, true, null, null, null, null, false);
				} else if (status.contains("SKIPPED")) {
					api.setTestCaseExecutionResult(testcase.getId(), null, Integer.parseInt(planId),
							ExecutionStatus.BLOCKED, null, null, comment, true, null, null, null, null, false);
				} else {
					api.setTestCaseExecutionResult(testcase.getId(), null, Integer.parseInt(planId),
							ExecutionStatus.BLOCKED, null, null, comment, true, null, null, null, null, false);
				}

			} catch (Exception e) {
			}
		}
	}

	/**
	 * Function to attach failure screenshots to corresponding TestLinkId
	 * 
	 * @param testcaseId
	 * @param fileName
	 */
	private void updateAttachementToTestLink(String testCaseId, String fileName) {
		String attach_screenshot_to_testlink = getProperty("./Config.properties", "attach_screenshot_to_testlink");
		testId.add(testCaseId);
		if (attach_screenshot_to_testlink.contains("Y")) {
			try {
				TestLinkAPI api = new TestLinkAPI(new URL(testLinkURL), testLinkDevKey);
				File attachmentFile = new File(fileName);
				String fileContent = null;
				try {
					byte[] byteArray = FileUtils.readFileToByteArray(attachmentFile);
					fileContent = new String(Base64.getEncoder().encodeToString(byteArray));
				} catch (IOException e) {
					Reporter.log("Unable to upload attachment :: Hence, quiting the code!!", true);
					emailResults.sendResultsMail(testId, null, null, 0, 0, 0,
							"Unable to upload attachment :: Hence, quiting the code!!", listOfBugs);
					throw new WrongInputException("Unable to upload attachment :: Hence, quiting the code!!");
				}
				TestCase testcase = api.getTestCaseByExternalId(testCaseId, 1);
				api.uploadTestCaseAttachment(testcase.getId(), fileName.substring(fileName.lastIndexOf('\\') + 1), null,
						fileName, null, fileContent);
				Reporter.log("File attached :::::::::::::::: " + fileName, true);

			} catch (Exception e) {
				Reporter.log("No file to attach ::::::::::::::::: ", true);
			}
		}

	}

	// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/**
	 * Mapping of data within sheets and execution of test steps are performed
	 * based on mechanism defined below. Execution proceeds based on name of
	 * Excel sheet passed to the function
	 * 
	 * @param workBookName
	 * @throws Exception
	 */
	private void execute_excel_test_cases(String workBookName) throws Exception {
		String workBookPath = pathToExcelFiles.concat(workBookName);
		excelData.setExcelFile(workBookPath);
		getSheetNames();
		int test_Cases_sheetIndex = excelsheetNames.indexOf("TestCase");
		excelData.setSheetToIndex(test_Cases_sheetIndex);
		int rowNumber = getRowIndexOfDesiredText("Test Case No.");
		int columnNumberOfDescription = getColumnIndexOfDesiredText("Description");
		int columnNumber = getColumnIndexOfDesiredText("TestSteps");
		testCaseDescription = excelData.getCellData(columnNumberOfDescription, rowNumber + 1);
		int lastValueofMergedRow = locationOfMergedRow.get(locationOfMergedRow.size() - 1);

		for (int i = rowNumber + 1; i < lastValueofMergedRow; i++) {
			String testStepValue = excelData.getCellData(columnNumber, i);
			if (testDataSelectionValue == null) {
				testDataSelectionValue = excelData.getCellData(columnNumber + 1, i);
			}
			if (!(testStepValue.isEmpty())) {
				Reporter.log("Test Step Value is :: " + testStepValue, true);
				excelData.setSheetToIndex(excelsheetNames.indexOf("TestSteps"));
				readRangeOfData(testStepValue);
				excelData.setSheetToIndex(test_Cases_sheetIndex);
				dataObjectMapping();
				validationObjectMapping();
				locatorObjectMapping();
				performFinalAction();
				excelData.setSheetToIndex(test_Cases_sheetIndex);
			}
		}

	}

	/**
	 * Function to get names of all sheets within a workbook
	 * 
	 */
	private void getSheetNames() {
		int sheetCount = excelData.getTotalNumberOfSheets();
		excelsheetNames = new ArrayList<String>();
		for (int i = 0; i < sheetCount; i++) {
			String sheetName = excelData.getSheetName(i);
			excelsheetNames.add(sheetName);
		}
	}

	/**
	 * Function to get Row Index of a cell based on data within the cell
	 * 
	 * @param text
	 * @return Returns row index of integer type
	 * @throws Exception
	 */
	private int getRowIndexOfDesiredText(String text) throws Exception {
		int rowNumber = 0;
		int rowCount = excelData.rows();
		int columnCount = excelData.getColumnCount();
		for (int i = 0; i < columnCount; i++) {
			for (int j = 0; j < rowCount; j++) {
				if (excelData.getCellData(i, j).toLowerCase().trim().contains(text.toLowerCase().trim())) {
					rowNumber = j;
				}
			}
		}
		return rowNumber;
	}

	/**
	 * Function to get Column Index of a cell based on data within the cell
	 * 
	 * @param text
	 * @return Returns column index of integer type
	 * @throws Exception
	 */
	private int getColumnIndexOfDesiredText(String text) throws Exception {
		int columnNumber = 0;
		int rowCount = excelData.rows();
		int columnCount = excelData.getColumnCount();

		for (int i = 0; i < columnCount; i++) {
			for (int j = 0; j < rowCount; j++) {
				if ((excelData.getCellData(i, j)).toLowerCase().trim().contains(text.toLowerCase().trim())) {
					columnNumber = i;
				}
			}
		}
		return columnNumber;
	}

	/**
	 * Function to read data from "TestSteps" sheet based on TestSteps mentioned
	 * on the "TestCase" sheet
	 * 
	 * @param Takes
	 *            TestSteps data as parameter defined on the TestCase sheet
	 * @throws Exception
	 */
	private void readRangeOfData(String testCaseActions) throws Exception {
		int execution = getRowIndexOfDesiredText(testCaseActions);
		int objectColumnIndex = getColumnIndexOfDesiredText("Object");
		int actionColumnIndex = getColumnIndexOfDesiredText("Action");
		int dataColumnIndex = getColumnIndexOfDesiredText("Data");
		int validationColumnIndex = getColumnIndexOfDesiredText("Validations");
		getIndexOfMergedRow();

		for (int j = execution + 1; j < locationOfMergedRow.get(locationOfMergedRow.size() - 1) - 1; j++) {
			testSteps_Object.add(excelData.getCellData(objectColumnIndex, j));
			testSteps_Actions.add(excelData.getCellData(actionColumnIndex, j));
			testSteps_Data.add(excelData.getCellData(dataColumnIndex, j));
			validationSet.add(excelData.getCellData(validationColumnIndex, j));
		}
	}

	/**
	 * Function to map Test data mentioned in the "TestData" sheet to the
	 * logical data name mentioned in the "TestSteps" sheet
	 * 
	 * @throws Exception
	 */
	private void dataObjectMapping() throws Exception {
		excelData.setSheetToIndex(excelsheetNames.indexOf("TestData"));
		dataSetList = new HashMap<>();
		int columnNum = getColumnIndexOfDesiredText(testDataSelectionValue);
		for (int data = 0; data < testSteps_Data.size(); data++) {
			String j = testSteps_Data.get(data);
			int logicalObjectDataRowNum = getRowNumberForResponseObject(j, 2, 0);
			if (logicalObjectDataRowNum == 0) {
				Reporter.log("Unable to find data \"" + j + "\" in Test Data sheet", true);
			} else {
				String dataValue = excelData.getCellData(columnNum, logicalObjectDataRowNum);
				dataSetList.put(j, dataValue);
			}
		}
	}

	/**
	 * Function to map Test data mentioned in the "TestData" sheet to the
	 * logical validation name mentioned in the "TestSteps" sheet
	 * 
	 * @throws Exception
	 */
	private void validationObjectMapping() throws Exception {
		excelData.setSheetToIndex(excelsheetNames.indexOf("TestData"));
		int columnNum = getColumnIndexOfDesiredText(testDataSelectionValue);

		for (int data = 0; data < validationSet.size(); data++) {
			String j = validationSet.get(data);
			int logicalObjectDataRowNum = getRowNumberForResponseObject(j, 2, 0);
			if (logicalObjectDataRowNum == 0) {
				Reporter.log("Unable to find validation \"" + j + "\" in Test Data sheet", true);
			} else {
				String dataValue = excelData.getCellData(columnNum, logicalObjectDataRowNum);
				validationObjectTestDataMap.put(j, dataValue);
			}
		}
	}

	/**
	 * Function to map identifiers mentioned in the "Identifier" sheet to the
	 * logical Object name mentioned in the "TestSteps" sheet
	 * 
	 * @throws Exception
	 */
	private void locatorObjectMapping() throws Exception {
		webElementListForTestSteps_Object = new HashMap<>();

		for (int object = 0; object < testSteps_Object.size(); object++) {
			String j = testSteps_Object.get(object);
			int logicalObjectDataRowNum = getRowNumberForResponseObject(j, 3, 1);
			if (logicalObjectDataRowNum == 0) {
				Reporter.log("Unable to find identifier for \"" + j + "\" in Identifier sheet", true);
			} else {
				String locatorType = excelData.getCellData(2, logicalObjectDataRowNum);
				String identifier = excelData.getCellData(3, logicalObjectDataRowNum);
				webElementListForTestSteps_Object.put(j, getLocators(locatorType, identifier));
			}
		}
	}

	/**
	 * Function consisting of all actions defined under Switch case. Based on
	 * the actions mentioned in the "TestSteps" sheet corresponding action is
	 * performed. More actions can be defined based on the requirements
	 * 
	 * @throws Exception
	 */

	private void performFinalAction() throws Exception {
		performValidations();
		for (int i = 0; i < testSteps_Object.size(); i++) {
			testSteps_action = testSteps_Actions.get(i);
			individualElement = webElementListForTestSteps_Object.get(testSteps_Object.get(i));

			switch (testSteps_action) {

			case "input":
				testSteps_testDataLogicalName = testSteps_Data.get(i);
				try {
					if (storeDynamicData.containsKey(testSteps_testDataLogicalName)) {
						testData_testDataToBePassed = storeDynamicData.get(testSteps_testDataLogicalName);
					} else {
						testData_testDataToBePassed = dataSetList.get(testSteps_testDataLogicalName);
					}
					waitForElementToBeVisible(CreateWebelement(individualElement));

					WebElement textBoxElement = createElementWithParameters(individualElement, testSteps_Object.get(i),
							"input", testData_testDataToBePassed);
					EnterData(textBoxElement, testData_testDataToBePassed);

					Reporter.log(testData_testDataToBePassed + " has been entered in text field : "
							+ testSteps_Object.get(i), true);
					beanForInfo.setStatus(StringContainer.pass);
					beanInfo.setComment(testData_testDataToBePassed + " has been entered in text field : "
							+ testSteps_Object.get(i));
				} catch (Exception e) {
					beanForInfo.setStatus(StringContainer.fail);
					beanInfo.setComment(testData_testDataToBePassed + " has not been entered in text field : "
							+ testSteps_Object.get(i));
				}
				break;

			case "click":
				testSteps_testDataLogicalName = testSteps_Data.get(i);
				testData_testDataToBePassed = dataSetList.get(testSteps_testDataLogicalName);
				try {
					if (individualElement.toString().contains("#")) {
						if (storeDynamicData.containsKey(testSteps_testDataLogicalName)) {
							individualElement = getLocators("xpath",
									individualElement.toString()
											.replace("#", storeDynamicData.get(testSteps_testDataLogicalName))
											.replace("By.xpath:", "").trim());
						} else {
							individualElement = getLocators("xpath", individualElement.toString()
									.replace("#", testData_testDataToBePassed).replace("By.xpath:", "").trim());
						}
					}
					waitForElementToBeVisible(CreateWebelement(individualElement));

					WebElement elementToBeClicked = createElementWithParameters(individualElement,
							testSteps_Object.get(i), "", "");
					ClickOnElement(elementToBeClicked);

					Reporter.log("Click has been performed on element : " + testSteps_Object.get(i), true);
					beanForInfo.setStatus(StringContainer.pass);
					beanInfo.setComment("Click has been performed on element : " + testSteps_Object.get(i));

					// Call validation function
					String validationName = validationObjectTestDataMap
							.get(validationsSet.get(testSteps_Object.get(i)));
					validationOperations(validationName, testSteps_Object.get(i));

					if (beanForInfo.getStatus().contains("FAIL"))
						beanForInfo.setComment(
								"Click performed on element " + testSteps_Object.get(i) + " but validation failed");
				} catch (Exception e) {
					beanForInfo.setComment(
							"Click performed on element " + testSteps_Object.get(i) + " but validation failed");
					beanForInfo.setStatus(StringContainer.fail);
				}
				break;

			case "getClientIdFromURL":
				String generateLogicalKey = "#" + nameOfPreRequisiteInDownloadedFile.replace(".xls", "").trim() + "_"
						+ (i + 1);
				Reporter.log("Logical Key ::::  " + generateLogicalKey, true);
				String getClientURL = getCurrentURL();
				dynamicValue_clientId = getClientURL.substring(getClientURL.lastIndexOf("=") + 1);
				storeDynamicData.put(generateLogicalKey, dynamicValue_clientId);
				Reporter.log("Client Id generated is : " + dynamicValue_clientId, true);
				beanForInfo.setStatus(StringContainer.pass);
				beanForInfo.setComment("Client Id generated is : " + dynamicValue_clientId
						+ " and is stored within logicalName : " + generateLogicalKey);
				break;
				
			case "Macro_01":
				System.out.println("Inside Macro_01");
				Reporter.log(" Inside Macro_01 ::::  ",true);
				break;

			case "select":
				testSteps_testDataLogicalName = testSteps_Data.get(i);
				testData_testDataToBePassed = dataSetList.get(testSteps_testDataLogicalName);
				waitForElementToBeVisible(CreateWebelement(individualElement));

				WebElement selectDropDownElement = createElementWithParameters(individualElement,
						testSteps_Object.get(i), "select", testData_testDataToBePassed);
				selectProvidedTextFromDropDownByValue(selectDropDownElement, testData_testDataToBePassed);
				break;
			}

			statusList.add(beanForInfo.getStatus());

			if (beanForInfo.getStatus().contains("FAIL")) {
				String createBugFlag = getProperty("./Config.properties", "createBugFlag");
				createBugFlag = System.getProperty("createBugFlag", createBugFlag);
				failCount++;
				stepNameList.add(testSteps_Object.get(i));
				finalcommentList.add(beanForInfo.getComment());
				String bugSummary = testCaseDescription.concat(" -- "+testSteps_Object.get(i));
				
				if(createBugFlag.contains("Y")){
				String bugId = jiraBugFinder.check_create_Bug(bugSummary);				
				listOfBugs.add(bugId);
				}				
				attachment_resultsFilePath = customFunction.takeScreenshot(takeScreenshotStatus, nameOfPreRequisiteInDownloadedFile);
				resultExcelData.setValues(rowForExcelSheeetMain, beanForInfo.getStatus(), listOfBugs.toString(),
					      beanForInfo.getComment(), testSteps_Object.get(i));
			} else if (beanForInfo.getStatus().contains("PASS")) {
				passCount++;
				resultExcelData.setValues(rowForExcelSheeetMain, beanForInfo.getStatus(), "", beanForInfo.getComment(),
						testSteps_Object.get(i));
			}

			rowForExcelSheeetMain++;
		}
	}

	/**
	 * All Validations related functions are called within a single function and
	 * is initialized before the Switch case for actions is started
	 * 
	 * @throws Exception
	 */
	private void performValidations() throws Exception {
		readValidationData();
		readValidationActions();
		generateMapOfPageObjectLogicalNameWithObjectsInIdentifiersSheet();
		generateMapbetweenPageObjectAndValidationNumber();
		validationObjectLogicalNameMapping();
		validationPageObjectLogicalNameMapping();
	}

	/**
	 * Function consisting of all actions defined for validations. Based on the
	 * actions mentioned in the "Validations" sheet corresponding action is
	 * performed. More actions can be defined based on the requirements
	 * 
	 * @param Takes
	 *            Validation logical name as parameter and Object name
	 *            corresponding to which the Validation is called
	 * @param objectName
	 */
	private void validationOperations(String validationMultipleValues, String objectName) {
		hardWait(4);
		// get validation number
		String[] multipleValidations;
		if (!(validationMultipleValues == null)) {
			multipleValidations = validationMultipleValues.split(",");
			for (int i = 0; i < multipleValidations.length; i++) {
				Reporter.log("---------------------------------------------------------------", true);
				Reporter.log("Validation started for following verification number :" + multipleValidations[i], true);
				Reporter.log("---------------------------------------------------------------", true);
				String validationActionName = ValidationActions.get(multipleValidations[i]);

				switch (validationActionName) {

				case "getURLContains":
					String validation = validationData.get(multipleValidations[i]);

					if (storeDynamicData.containsKey(validation)) {
						if (getCurrentURL().contains(storeDynamicData.get(validation))) {
							Reporter.log("Current URL " + getCurrentURL() + " contains validation data :: "
									+ storeDynamicData.get(validation), true);
							beanForInfo.setStatus(StringContainer.pass);
							beanForInfo.setComment(
									"Current URL contains validation data :: " + storeDynamicData.get(validation));
						} else {
							Reporter.log("Current URL " + getCurrentURL() + " does not contains validation data :: "
									+ validation, true);
							beanForInfo.setComment("Current URL " + getCurrentURL()
									+ " does not contains validation data :: " + validation);
							beanForInfo.setStatus(StringContainer.fail);
						}
					} else {
						if (getCurrentURL().contains(validation)) {
							Reporter.log("Current URL contains validation data :: " + validation, true);
							beanForInfo.setStatus(StringContainer.pass);
							beanForInfo.setComment("Current URL contains validation data :: " + validation);
						} else {
							Reporter.log("Current URL " + getCurrentURL() + " does not contains validation data :: "
									+ validation, true);
							beanForInfo.setComment("Current URL " + getCurrentURL()
									+ " does not contains validation data :: " + validation);
							beanForInfo.setStatus(StringContainer.fail);
						}
					}
					break;

				case "getText":
					WebElement elementText;
					String elementGetTextValue, validationValueKey, expectedValue;
					try {
						validationValueKey = validationNumberIdentifier.get(multipleValidations[i]);
						expectedValue = validationData.get(multipleValidations[i]);

						if (storeDynamicData.containsKey(validationValueKey)) {
							validationValueKey = storeDynamicData.get(validationValueKey);
						} else if (dataSetList.containsKey(validationValueKey)) {
							validationValueKey = dataSetList.get(validationValueKey);
						}

						By locatorValue = validationsPageObject_IdentifiersLocator.get(validationValueKey);
						waitForElementToBeVisible(CreateWebelement(locatorValue));

						elementText = createElementWithParameters(locatorValue, objectName, "", "");
						elementGetTextValue = GetText(elementText);

						if (elementGetTextValue.contains(expectedValue)) {
							beanForInfo.setStatus(StringContainer.pass);
							Reporter.log("Click performed on element and getText operation on : " + validationValueKey
									+ " results in " + elementGetTextValue, true);
						} else {
							beanForInfo.setComment(
									"Click performed on element  validation failed on : " + multipleValidations[i]);
							beanForInfo.setStatus(StringContainer.fail);
						}
					} catch (Exception e) {
						Reporter.log("Click performed on element but getText operation failed on : "
								+ multipleValidations[i], true);
						beanForInfo.setComment(
								"Click performed on element but validation failed on : " + multipleValidations[i]);
						beanForInfo.setStatus(StringContainer.fail);
					}
					break;

				case "isElementDisplayed":
					By locatorValue = validationsPageObject_IdentifiersLocator
							.get(validationNumberIdentifier.get(multipleValidations[i]));
					String validationValueKey2 = validationData.get(multipleValidations[i]);

					if (locatorValue.toString().contains("#")) {
						if (storeDynamicData.containsKey(validationValueKey2)) {

							locatorValue = getLocators("xpath",
									locatorValue.toString().replace("#", storeDynamicData.get(validationValueKey2))
											.replace("By.xpath: ", "").trim());
						} else if (dataSetList.containsKey(validationValueKey2)) {
							locatorValue = getLocators("xpath",
									locatorValue.toString().replace("#", dataSetList.get(validationValueKey2))
											.replace("By.xpath: ", "").trim());
						}
					}

					WebElement element = createElementWithParameters(locatorValue, objectName, "", "");

					try {
						boolean check = isElementDisplayed(element, objectName);
						if (check) {
							Reporter.log(objectName + " is displayed", true);
							beanForInfo.setStatus(StringContainer.pass);
							beanInfo.setComment(objectName + " is dislayed");
						} else {
							Reporter.log(objectName + " is not displayed", true);
							beanForInfo.setStatus(StringContainer.fail);
							beanInfo.setComment(objectName + " is not dislayed");
						}
					} catch (Exception e) {
						Reporter.log(objectName + " element is not displayed", true);
						beanForInfo.setStatus(StringContainer.fail);
						beanInfo.setComment(objectName + " is not dislayed");
					}
					break;

				case "isElementNotDisplayed":
					By locatorValue1 = validationsPageObject_IdentifiersLocator
							.get(validationNumberIdentifier.get(multipleValidations[i]));
					String validationValueKey1 = validationData.get(multipleValidations[i]);

					if (locatorValue1.toString().contains("#")) {
						if (storeDynamicData.containsKey(validationValueKey1)) {
							locatorValue1 = getLocators("xpath",
									locatorValue1.toString().replace("#", storeDynamicData.get(validationValueKey1))
											.replace("By.xpath: ", "").trim());
						}
					}

					WebElement element1 = createElementWithParameters(locatorValue1, objectName, "", "");
					try {
						boolean check = isElementNotDisplayed(element1, objectName);
						if (check) {
							Reporter.log(objectName + " is displayed", true);
							beanForInfo.setStatus(StringContainer.fail);
							beanInfo.setComment(objectName + " is dislayed");
						} else {
							Reporter.log(objectName + " is not displayed", true);
							beanForInfo.setStatus(StringContainer.pass);
							beanInfo.setComment(objectName + " is not displayed");
						}
					} catch (Exception e) {
						Reporter.log(objectName + " element is not displayed", true);
						beanForInfo.setStatus(StringContainer.pass);
						beanInfo.setComment(objectName + " is not displayed");
					}
					break;
				}
				finalcommentList.add(beanForInfo.getComment());
				statusList.add(beanForInfo.getStatus());
				if (beanInfo.getStatus() == "FAIL") {
					attachment_resultsFilePath = customFunction.takeScreenshot(takeScreenshotStatus,
							nameOfPreRequisiteInDownloadedFile);
				}
			}
		}
	}

	/**
	 * Read validation data corresponding to the Validation No. mentioned in
	 * "Validation" sheet
	 * 
	 * @throws Exception
	 */
	private void readValidationData() throws Exception {
		excelData.setSheetToIndex(excelsheetNames.indexOf("Validations"));
		int actionColumnIndex = getColumnIndexOfDesiredText("Validation Data");
		int cloumnForObjects = getColumnIndexOfDesiredText("Validation No.");

		for (int i = 1; i <= excelData.rows(); i++) {
			validationData.put(excelData.getCellData(cloumnForObjects, i), excelData.getCellData(actionColumnIndex, i));
		}
	}

	/**
	 * Read validation actions corresponding to the Validation No. mentioned in
	 * "Validation" sheet
	 * 
	 * @throws Exception
	 */
	private void readValidationActions() throws Exception {
		excelData.setSheetToIndex(excelsheetNames.indexOf("Validations"));
		int actionColumnIndex = getColumnIndexOfDesiredText("Action");
		int cloumnForObjects = getColumnIndexOfDesiredText("Validation No.");

		for (int i = 1; i <= excelData.rows(); i++) {
			ValidationActions.put(excelData.getCellData(cloumnForObjects, i),
					excelData.getCellData(actionColumnIndex, i));
		}
	}

	/**
	 * Generates a map of Page Object logical name mentioned in the
	 * "Validations" sheet to corresponding identifier mentioned on the
	 * "Identifier" sheet. Here "Page Object Logical Name" acts as 'Key' and
	 * "identifier" as 'Value'
	 * 
	 * @throws Exception
	 */
	private void generateMapOfPageObjectLogicalNameWithObjectsInIdentifiersSheet() throws Exception {
		excelData.setSheetToIndex(excelsheetNames.indexOf("Validations"));
		int getColumofPageObject = getColumnIndexOfDesiredText("Page Object Logical Name");

		for (int i = 1; i <= excelData.rows(); i++) {
			String value = excelData.getCellData(getColumofPageObject, i);

			if (!(value.isEmpty())) {
				excelData.setSheetToIndex(excelsheetNames.indexOf("Identifier"));
				int rowOfLogicalNameInIndentifierSheet = getRowIndexOfDesiredText(value);
				By validationBylocator = getLocators(excelData.getCellData(2, rowOfLogicalNameInIndentifierSheet),
						excelData.getCellData(3, rowOfLogicalNameInIndentifierSheet));
				validationsPageObject_IdentifiersLocator.put(value, validationBylocator);
			}
			excelData.setSheetToIndex(excelsheetNames.indexOf("Validations"));
		}
	}

	/**
	 * Generates map for Validation No. and corresponding Page Object Logical
	 * Name in the "Validations" sheet. Here, "Validation No." acts as 'Key' and
	 * "Page Object Logical Name" as 'Value'
	 * 
	 * @throws Exception
	 */
	private void generateMapbetweenPageObjectAndValidationNumber() throws Exception {
		excelData.setSheetToIndex(excelsheetNames.indexOf("Validations"));
		int getColumofPageObject = getColumnIndexOfDesiredText("Page Object Logical Name");
		int getColumofValidationNo = getColumnIndexOfDesiredText("Validation No.");

		for (int i = 1; i <= excelData.rows(); i++) {
			String key = excelData.getCellData(getColumofValidationNo, i);
			String value = excelData.getCellData(getColumofPageObject, i);
			validationNumberIdentifier.put(key, value);
		}
	}

	/**
	 * Generates map for Object Name and corresponding Validation No. in
	 * "TestSteps" sheet. Here, "Object Name" acts as 'Key' and "Validation No."
	 * as 'Value'
	 * 
	 * @throws Exception
	 */
	private void validationObjectLogicalNameMapping() throws Exception {
		excelData.setSheetToIndex(excelsheetNames.indexOf("TestSteps"));
		int cloumnForObjects = getColumnIndexOfDesiredText("Object");
		int cloumnForValidation = getColumnIndexOfDesiredText("Validations");

		for (int i = 1; i <= excelData.rows(); i++) {
			if (!(excelData.getCellData(cloumnForObjects, i).isEmpty()
					|| excelData.getCellData(cloumnForValidation, i).isEmpty())) {
				validationsSet.put(excelData.getCellData(cloumnForObjects, i),
						excelData.getCellData(cloumnForValidation, i));
			}
		}
	}

	/**
	 * Generates map for Validation No. and corresponding Page Object Logical
	 * Name in the "Validations" sheet. Here, "Validation No." acts as 'Key' and
	 * "Page Object Logical Name" as 'Value'
	 * 
	 * @throws Exception
	 */
	private void validationPageObjectLogicalNameMapping() throws Exception {
		excelData.setSheetToIndex(excelsheetNames.indexOf("Validations"));
		int cloumnForObjects = getColumnIndexOfDesiredText("Validation No.");
		int cloumnForValidation = getColumnIndexOfDesiredText("Page Object Logical Name");

		for (int i = 1; i <= excelData.rows(); i++) {
			validatePageObjectMap.put(excelData.getCellData(cloumnForObjects, i),
					excelData.getCellData(cloumnForValidation, i));
		}
	}

	/**
	 * Function to manipulate the Overall TestCase status based on status of
	 * individual Test Steps
	 * 
	 */
	private void manipulateStatusList() {
		if (statusList.contains("FAIL")) {
			testCaseExecutionStatus = "FAIL";
		} else {
			testCaseExecutionStatus = "PASS";
		}

		if (stepNameList.size() == 0)
			Reporter.log("Test Cases Status ::: " + testCaseExecutionStatus, true);
		else
			Reporter.log("Test Cases Status:::: " + testCaseExecutionStatus + " for step ::: " + stepNameList, true);

		Reporter.log("Comment For Test Case :::: " + finalcommentList, true);
	}

	/**
	 * Function to create Results file having Filename same as name of main
	 * excel file, current execution date and time
	 * 
	 * @param fileName
	 * @return Path to the results file
	 */
	private String createResultsFile(String fileName) {
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH-mm-ss");
		pathtoResultsFile = pathtoResultsFile + fileName + "_" + dateFormat.format(date) + ".xls";
		Reporter.log(("Path to Results file is :::: " + pathtoResultsFile), true);
		return pathtoResultsFile;
	}

	/**
	 * Function to get RowNumber based on celldata,sheet number and column
	 * passed
	 * 
	 * @param responseObject
	 * @param sheetNumber
	 * @param column
	 * @return Row number consisting of celldata
	 * @throws Exception
	 */
	private int getRowNumberForResponseObject(String responseObject, int sheetNumber, int column) throws Exception {
		excelData.setSheetToIndex(sheetNumber);
		int k = 0;

		for (int i = 1; i <= excelData.rows(); i++) {
			if (excelData.getCellData(column, i).contains(responseObject)) {
				k = i;
				break;
			} else {
				k = 0;
			}
		}
		return k;
	}

	/**
	 * Function to get Index of Merged rows
	 * 
	 * @throws Exception
	 */
	private void getIndexOfMergedRow() throws Exception {
		int region = excelData.getNumMergedRegions();
		locationOfMergedRow = new ArrayList<Integer>();

		for (int i = 0; i < region; i++) {
			int columnIndex = excelData.getMergedRegionColumn(i);
			int rowIndex = excelData.getMergedRegionRow(i);
			String getCellDataAtIndex = excelData.getCellData(columnIndex, rowIndex);
			if ((getCellDataAtIndex.isEmpty())) {
				locationOfMergedRow.add(rowIndex + 1);
			}
		}
	}

	/**
	 * Function to clear all Lists and Map created during the test execution
	 * 
	 */
	private void clearAllListsAndMap() {
		testSteps_Object.clear();
		testSteps_Actions.clear();
		testSteps_Data.clear();
		validationSet.clear();
		validationsSet.clear();
		dataSetList.clear();
		webElementListForTestSteps_Object.clear();
		validationsSet.clear();
		validatePageObjectMap.clear();
		ValidationActions.clear();
		validationList.clear();
		validationsPageObject_IdentifiersLocator.clear();
		validationNumberIdentifier.clear();
		statusList.clear();
		finalcommentList.clear();
		stepNameList.clear();
		validationObjectTestDataMap.clear();
		testId.clear();
	}
}
