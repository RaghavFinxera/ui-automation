package com.qait.finxera.test.executor;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import com.qait.automation.TestSessionInitiator;
import com.qait.automation.utils.WrongInputException;

public class TestExecutor {

	static TestSessionInitiator test;
	@SuppressWarnings("rawtypes")
	static Class actionKeywords;
	@SuppressWarnings("rawtypes")
	static Class actionKeyword1;
	static Field f;
	static Object var;
	static Method[] method;
	static Object objectTestClass;
	static WrongInputException exception;

	/**
	 * Execution class containing the Main function which calls the
	 * ExecutionKeywords.java class and executes the method written within
	 * 
	 * @param args
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws SecurityException
	 * @throws NoSuchFieldException
	 * @throws InvocationTargetException
	 * @throws IllegalArgumentException
	 * @throws WrongInputException
	 * @throws Exception
	 */
	public static void main(String args[]) throws WrongInputException, InstantiationException, IllegalAccessException, NoSuchFieldException, SecurityException, IllegalArgumentException, InvocationTargetException {
		
			actionKeywords = TestSessionInitiator.class;
			objectTestClass = actionKeywords.newInstance();
			f = actionKeywords.getDeclaredField("execution_keywords");
			var = f.get(objectTestClass);
			actionKeyword1 = var.getClass();
			method = actionKeyword1.getDeclaredMethods();
			for (int i = 0; i < method.length; i++) {
				if (method[i].getName().contains(("run_function"))) {
					method[i].invoke(var, objectTestClass);
				}
			}
		
			test = (TestSessionInitiator) objectTestClass;
			test.closeBrowserWindow();
				
		
	}
}
